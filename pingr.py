
'''
Execute a simple ping to another host on the (local) network
'''

from scapy.all import *

IP_VM = "10.0.2.15"
IP_HOST = "10.0.11.84"

pingr = IP(dst=IP_HOST)/ICMP()   # ping VM in same subnet

sr1(pingr)