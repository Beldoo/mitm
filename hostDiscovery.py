import sys
from datetime import datetime
from scapy.all import *


'''
Executes a scan of active hosts on the network
'''

def layer3Scan():
    try:
        interface = "myInterface" #input("[*] Enter Interface: ")
        ipRange = "10.0.11.160"
    except KeyboardInterrupt:
        print("\n [*] User invoked shutdown.")
        print("[*] Exiting...")
        sys.exit(1)

    print("\n[*] Scanning...")
    start_time = datetime.now()
    conf.verb = 0

    for ip in range(0, 5):
        packet = IP(dst="10.0.11." + str(ip))/ICMP()
        reply = sr1(packet, timeout=2)
        if not (reply is None):
            print(reply.dst, "is online") # vom zurückkommenden Paket
            print(reply.src)  # vom zurückkommenden Paket
        else:
                print("Timeout waiting for {0}".format(packet[IP].dst))

    # print("MAC - IP\n")
    # for snd, rcv in ans:
    #     print(ans.dst)#.sprint(r"%Ether.src% - %ARP.psrc%"))
    stop_time = datetime.now()
    total_time = stop_time - start_time

    print("\n[*] Scan complete.")
    print("\n[*] Scan Duration: {0}.".format(total_time))

#layer3Scan()

def layer2Scan():
    '''
    The function pings 
    '''
    try:
        _interface = input("[*] Enter Interface: ")
    except KeyboardInterrupt:
        print("\n [*] User invoked shutdown.")
        print("[*] Exiting...")
        sys.exit(1)

    print("\n[*] Scanning...")
    start_time = datetime.now()
    conf.verb = 0

    ipAddress = ipRange(verbose=0)
    #print(ipAddress)
    for ip in ipAddress:
        _pkt = Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=ip)
        ans, unans = srp( _pkt, iface=_interface, timeout=2, verbose=True)
        for snt, recv in ans:
            if recv:
                print("Host Alive: {0} - {1}" % (recv[ARP].psrc, recv[Ether].src))
            else:
                print("Timeout for {0}".format(ip))

def ipRange(verbose=1):
    '''
    Function creates all IP addresses you want to ping
    '''

    print("ipRange() läuft...")
    _range = input("Enter the Target Range: ") # 192.168.1.1/32 means from 192.168.1.1 till 192.168.1.32
                                              # NO CIDR!!
    #_interface = input("Enter the network interface: ")
    ip, ntBits = _range.split('/')
    print("ip: {0}\nntBits: {1}".format(ip, ntBits)) if(verbose == 1) else 0
    ip_addresses = []  # empty list will be filled with IP addresses
    st_bit = ip.split('.')[3:4][0]   #Since it's an IPv4
    print("st_bit: {0}".format(st_bit)) if(verbose == 1) else 0
    for n in range(1, int(ntBits)+1):
        eval_ip = ".".join( ip.split('.')[:-1] ) + '.' + str(n)
        print("eval_ip: {0}".format(eval_ip)) if(verbose == 1) else 0
        ip_addresses.append( eval_ip )
    print("\nsuccess.")
    # output = [ip_addresses[n] for n in range(1,len(ip_addresses))]
    # print(output)
    #print(type(ip_addresses), "\n", ip_addresses)
    return ip_addresses

#ipRange(verbose=0)

# 192.168.1.1/32

layer2Scan()

## string manipulation:
ip = "192.168.0.1"
# print(ip.split('.')[:-1])  # splitting delimitor . -> element put in list
#                            # :-1 return all elements BUT last one
# print("abc".join("XYZ"))  # vorderer string wird iterativ angehangen

## inner functions:
def f1():
    var = f2(2)
    test = ipRange()
    print(test)

def f2(val):
    a = [1,2,3]
    b = [i*val for i in a]
    #print(b)
    c = "Hallo"
    return b

#f1()