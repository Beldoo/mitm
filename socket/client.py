#!/usr/bin/env python3

'''
Socket Client
'''

import socket

HOST = '10.0.11.84'  # The server's hostname or IP address
PORT = 65432        # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))  # verbindet sich mit Host
    while True:
        #s.sendall(b'Hello, world')  # sendet String an Host
        #data = s.recv(1024)
        text = input().encode()
        s.sendall(text)
    
#print('Received', repr(data))
#print("Hallo")