#!/usr/bin/env python3

'''
Socket Server
'''

import socket

HOST = '10.0.11.84'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))   # akzeptiert Verbungsaufnahmen an gegebene IP+Port
    s.listen()             # lauscht kontinuierlich
    conn, addr = s.accept()  # bei akzeptierten Verbundung werden 2 Var. rausgezogen
    with conn:
        print('addr: ', addr)
        #print('conn: ', conn)
        while True:
            data = conn.recv(1024)  # recv(1024) sind die empfangenen Daten
            print("Client:")
            print(data)
            if not data:
                break
            #conn.sendall(data)  # schicken empfangene Daten (data) zurück an Client