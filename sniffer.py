from scapy.all import *
import re

'''
Execute Packet Monitoring
Available:
- ARP
'''

#def sniffData(pkt):
    # if pkt.haslayer( Raw ):   # Check for the Data layer
    #     header = pkt.getlayer( Raw ).load    # Get the sent data
    #     if header.startswith('GET'):     # Make sure it's a request
    #         if '/search?' in header:
    #             q = re.search(r'q=(.*)\w', header)
    #             if q:
    #                 src = pkt.getlayer(IP).src
    #                 query = q.group().split(' ')[0].lstrip('q=')
    #                 print("{0} searched for {1}".format(src, query))

def arp_monitor(pkt):
    if pkt[ARP].op == 1: #who-has (request)
        print("Request: {0} asking about {1}".format(pkt[ARP].psrc, pkt[ARP].pdst))
    if pkt[ARP].op == 2: #is-at (response)
        print("*Response: {0} has address {1}".format(pkt[ARP].hwsrc, pkt[ARP].psrc))

def main():
    #conf.iface="virtSwitch"
    sniff(prn=arp_monitor, filter="arp", store=0, count=10)
    
    
if __name__ == '__main__':
    main()

# mac 2c:4d:54:d1:65:69